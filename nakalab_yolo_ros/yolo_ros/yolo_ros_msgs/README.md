# yolo_ros_msgs
　コアパッケージ [**`yolo_ros`**](../yolo_ros/) が使用するメッセージファイルおよび検出結果を内包するメッセージファイル群 **yolo_ros/msgs/msg/\*** を提供します。

## メッセージファイル一覧
### [ObjectPose3DArray](msg/ObjectPose3DArray.msg)
物体検出結果を内包したメッセージ **[ObjectPose3D](msg/ObjectPose3D.msg)** を内包したメッセージです。

### [ObjectPose3D](msg/ObjectPose3D.msg)
物体検出結果を含むメッセージです。このメッセージから検出した物体のクラス名と3次元的な物体の大きさ、位置、検出率を内包しています。

- **header** [Header](https://docs.ros2.org/latest/api/std_msgs/msg/Header.html)<br>
    物体検出した時点のタイムスタンプと物体検出に使用したカメラ frame_id を含みます。

- **frame_id** [string](https://docs.ros2.org/latest/api/std_msgs/msg/String.html)<br>
    検出した物体の名前

- **class_id** [int32](https://docs.ros2.org/latest/api/std_msgs/msg/Int32.html)<br>
    検出した物体のクラスID

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出した物体の検出確率

- **center** [Pose](https://docs.ros2.org/latest/api/geometry_msgs/msg/Pose.html)<br>
    検出した物体の中心座標。座標は物体検出に使用したカメラ `header.frame_id` からの相対座標です。

- **size** [Vector3](https://docs.ros2.org/latest/api/geometry_msgs/msg/Vector3.html)<br>
    検出した物体の推定サイズ [x, y, z] (m) を内包しています。

### [ObjectPose2DArray](msg/ObjectPose2DArray.msg)
画像データからの物体検出結果を内包したメッセージ **[ObjectPose2D](msg/ObjectPose2D.msg)** を内包したメッセージです。

### [ObjectPose2D](msg/ObjectPose2D.msg)
物体検出結果を含むメッセージです。このメッセージから検出した物体のクラス名と画像ピクセル座標、検出結果などを内包します。

- **segment_image** [Image](https://docs.ros2.org/latest/api/sensor_msgs/msg/Image.html)<br>
    `segment` が有効な場合、このフィールドに検出された物体のマスキング画像（`mono8`）が格納されます。

- **class_name** [string](https://docs.ros2.org/latest/api/std_msgs/msg/String.html)<br>
    検出した物体の名前

- **class_id** [int32](https://docs.ros2.org/latest/api/std_msgs/msg/Int32.html)<br>
    検出した物体のクラスID

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出した物体の検出確率

- **pose** [Vector2D](msg/Vector2D.msg)<br>
    検出した物体の中心画像ピクセル座標[x, y] (pix)を内包しています。中心座標は`segment` が有効な場合、セグメント画像を用いて検出した物体の重心座標が格納されます。

### [PersonPose3DArray](msg/PersonPose3DArray.msg)
人体検出結果を内包したメッセージ **[PersonPose3D](msg/PersonPose3D.msg)** を内包したメッセージです。

### [PersonPose3D](msg/PersonPose3D.msg)
人体検出結果を含むメッセージです。このメッセージから検出した人体の3次元的な座標、人体の大きさ、各関節キーポイントの座標、検出率を内包しています。

- **keypoints** [KeyPoint3DArray](msg/KeyPoint3DArray.msg)<br>
    人体検出した時に取得する各関節キーポイント群を内包しています。

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出した人体の検出確率

- **center** [Pose](https://docs.ros2.org/latest/api/geometry_msgs/msg/Pose.html)<br>
    検出した人体の中心座標。座標は人体検出に使用したカメラ `header.frame_id` からの相対座標です。

- **size** [Vector3](https://docs.ros2.org/latest/api/geometry_msgs/msg/Vector3.html)<br>
    検出した人体の推定サイズ [x, y, z] (m) を内包しています。

### [KeyPoint3DArray](msg/KeyPoint3DArray.msg)
人体検出時に取得したキーポイント群 [KeyPoint3D](msg/KeyPoint3D.msg) を格納するメッセージです。

### [KeyPoint3D](msg/KeyPoint3D.msg)
　人体の骨格キーポイント座標を格納しています。

- **id** [int32](https://docs.ros2.org/latest/api/std_msgs/msg/Int32.html)<br>
    キーポイント ID で、検出した関節部に対応しています。各 ID と関節との関係は [COCO データ](https://mmpose.readthedocs.io/en/latest/dataset_zoo/2d_body_keypoint.html#coco)
    を参照してください。関節と ID の関係を示す図があります。

- **point** [Point](https://docs.ros2.org/latest/api/geometry_msgs/msg/Point.html)<br>
    検出したキーポイントの3次元座標 [x, y, z]

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出したキーポイントの検出確率

### [PersonPose2DArray](msg/PersonPose2DArray.msg)
人体検出結果を内包したメッセージ **[PersonPose2D](msg/PersonPose2D.msg)** を内包したメッセージです。

### [PersonPose2D](msg/PersonPose2D.msg)
人体検出結果を含むメッセージです。このメッセージから検出した人体の画像ピクセル座標、各関節キーポイントの座標、検出率を内包しています。

- **keypoints** [KeyPoint2DArray](msg/KeyPoint2DArray.msg)<br>
    人体検出した時に取得する各関節キーポイント群を内包しています。（2次元）

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出した人体の検出確率

- **pose** [Vector2D](msg/Vector2D.msg)<br>
    検出した人体の中心画像ピクセル座標[x, y] (pix)を内包しています。

### [KeyPoint2DArray](msg/KeyPoint2DArray.msg)
人体検出時に取得したキーポイント群 [KeyPoint2D](msg/KeyPoint2D.msg) を格納するメッセージです。

### [KeyPoint2D](msg/KeyPoint2D.msg)
　人体の骨格キーポイント座標を格納しています。

- **id** [int32](https://docs.ros2.org/latest/api/std_msgs/msg/Int32.html)<br>
    キーポイント ID で、検出した関節部に対応しています。各 ID と関節との関係は [COCO データ](https://mmpose.readthedocs.io/en/latest/dataset_zoo/2d_body_keypoint.html#coco)
    を参照してください。関節と ID の関係を示す図があります。

- **predict** [float64](https://docs.ros2.org/latest/api/std_msgs/msg/Float64.html)<br>
    検出したキーポイントの検出確率

- **pose** [Vector2D](msg/Vector2D.msg)<br>
    検出した人キーポイントの画像ピクセル座標[x, y] (pix)を内包しています。

### [Vector2D](msg/Vector2D.msg)
　ピクセル座標 [x, y] を格納するメッセージです。