#!/usr/bin/env python3#!/usr/bin/env python3
from launch import LaunchDescription
from launch_ros.actions import Node, PushRosNamespace
from launch.actions import ExecuteProcess, RegisterEventHandler, LogInfo, OpaqueFunction
from launch.conditions import IfCondition
from launch.event_handlers import OnProcessStart
from launch.actions import DeclareLaunchArgument, GroupAction
from launch.substitutions import LaunchConfiguration

from ament_index_python.packages import get_package_share_directory
import json
import os

def generate_launch_description():
    # 自動実行の設定を行う関数
    def create_auto_executor(context):
        # 'auto_execute'引数が'true'の場合、サービスコールを設定
        if LaunchConfiguration('auto_execute').perform(context) == 'true':
            return [RegisterEventHandler(
                OnProcessStart(
                    target_action=yolo_ros_core,
                    on_start=[
                        LogInfo(msg="==== Auto execute enabled. YOLO will start ===="),  # 自動実行が有効であることをログ出力
                        service_call_cmd  # サービスコールコマンドを実行
                    ]
                )
            )]
        return []
        
    # YOLO ROS Coreノードの設定
    yolo_ros_core = Node(package='yolo_ros',
                            executable='yolo_ros_core',
                            name='yolo_ros_core',
                            remappings=[
                                ('/image_raw', LaunchConfiguration('image_raw'))  # 'image_raw'トピックのリマッピング
                            ],
                            parameters=[
                                {'debug': LaunchConfiguration('debug')},  # デバッグパラメータの設定
                                {'yolo_model': LaunchConfiguration('yolo_model')}
                    ])
                    
    # サービスコールコマンドの設定
    service_call_cmd = ExecuteProcess(
        cmd=['ros2', 'service', 'call',
            '/yolo_ros/detect_persons/execute_predict',
            'std_srvs/srv/SetBool',
            json.dumps({"data": True})  # サービスコールの引数をJSON形式で設定
        ]
    )
    
    return LaunchDescription([
        # Launch引数の宣言
        DeclareLaunchArgument(
            'yolo_model',
            default_value='yolov8n-pose.pt',
            description='Remap the image raw topic'  # 使用するデフォルトの YOLO モデル
        ),
        DeclareLaunchArgument(
            'image_raw',
            default_value='/rgb/image_raw',
            description='Remap the image raw topic'  # image_rawトピックのリマッピング
        ),
        DeclareLaunchArgument(
            'depth_image',
            default_value='/depth/image_raw',
            description='Remap the depth image topic'  # depth_imageトピックのリマッピング
        ),
        DeclareLaunchArgument(
            'depth_camera_info',
            default_value='/depth/camera_info',
            description='Remap the depth camera info topic'  # depth_camera_infoトピックのリマッピング
        ),
        DeclareLaunchArgument(
            'debug',
            default_value='false',
            description='Debug parameter'  # デバッグパラメータ
        ),
        DeclareLaunchArgument(
            'use_viz',
            default_value='false',
            description='Visualize yolo detection via RViz'  # RVizによるYOLO検出の可視化
        ),
        DeclareLaunchArgument(
            'auto_execute',
            default_value='false',
            description='Enable execute YOLO via service call'  # サービスコールによるYOLO実行の有効化
        ),
        # YOLO ROS Coreノードの追加
        yolo_ros_core,
        # 3Dオブジェクト検出ノードの設定
        Node(package='yolo_ros',
            executable='person_detect3d',
            name='person_detect3d',
            remappings=[
                ('/depth_image', LaunchConfiguration('depth_image')),  # depth_imageトピックのリマッピング
                ('/depth_camera_info', LaunchConfiguration('depth_camera_info'))  # depth_camera_infoトピックのリマッピング
            ]),
        # YOLO可視化ノードの設定
        Node(package='yolo_ros',
            executable='person_detection_visualizer',
            name='yolo_viz'),
        # 自動実行設定の追加
        OpaqueFunction(function=create_auto_executor),
        # RVizノードの設定（use_vizがtrueの場合のみ実行）
        Node(
            package="rviz2",
            executable="rviz2",
            condition=IfCondition(LaunchConfiguration('use_viz')),
            arguments=["-d", os.path.join(
                get_package_share_directory('yolo_ros'),
                'rviz', 'yolo_ros.rviz'  # RViz設定ファイルのパス
            )]
        )
    ])

