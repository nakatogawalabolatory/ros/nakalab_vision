#!/usr/bin/env python3

from rclpy.qos import QoSProfile
from rclpy.qos import QoSHistoryPolicy
from rclpy.qos import QoSDurabilityPolicy
from rclpy.qos import QoSReliabilityPolicy
from rclpy.node import Node
import rclpy

from std_srvs.srv import SetBool
from sensor_msgs.msg import Image
from yolo_ros_msgs.msg import ObjectPose2D, ObjectPose2DArray
from yolo_ros_msgs.msg import KeyPoint2D, KeyPoint2DArray
from yolo_ros_msgs.msg import PersonPose2D, PersonPose2DArray

from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np

from ultralytics import YOLO
import torch

class YoloCore(Node):
    def __init__(self):
        super().__init__('yolo_ros_core')

        # execute flag
        self.execute_flag = False

        # Qos
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            depth=1
        )

        # cv bridge
        self.bridge = CvBridge()

        # param
        self.declare_parameter('yolo_model', 'yolov9c-seg.pt')
        self.declare_parameter('minimum_predict', 0.5)
        self.declare_parameter('debug', True)

        self.yolo_model = self.get_parameter('yolo_model').get_parameter_value().string_value
        self.minimum_predict = self.get_parameter('minimum_predict').get_parameter_value().double_value
        self.debug = self.get_parameter('debug').get_parameter_value().bool_value
        # pub & sub
        self._sub = self.create_subscription(Image, "image_raw", self._image_cb, qos_profile)
        # srv
        
        if "pose" in self.yolo_model:
            self._image_pub = self.create_publisher(Image, "/yolo_ros/detect_persons/image", 10)
            self._flag_srv = self.create_service(SetBool, '/yolo_ros/detect_persons/execute_predict', self._srv_cb)
            self._objects_pub = self.create_publisher(PersonPose2DArray, "/yolo_ros/detect_persons", 10)
        else:
            self._image_pub = self.create_publisher(Image, "/yolo_ros/detect_objects/image", 10)
            self._flag_srv = self.create_service(SetBool, '/yolo_ros/detect_objects/execute_predict', self._srv_cb)
            self._objects_pub = self.create_publisher(ObjectPose2DArray, "/yolo_ros/detect_objects", 10)
            
        self.yolo = YOLO(self.yolo_model)

        self.get_logger().info(f"Wait for activating {self.get_name()}. use {self.yolo_model}")

    def _srv_cb(self, req, res):
        self.execute_flag = req.data
        res.success = req.data

        if req.data:
            message = f"Activating {self.get_name()}. use {self.yolo_model}"
        else:
            message = f"Stop {self.get_name()}."
        res.message = message
        self.get_logger().info(message)

        return res
    
    def destroy(self):
        self.get_logger().info(f"Shutdown {self.get_name()}")
        cv2.destroyAllWindows()
        self.destroy_node()

    def _image_cb(self, msg: Image) :
        if self.execute_flag:
            try:
                image = self.bridge.imgmsg_to_cv2(msg)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            except CvBridgeError as e :
                self.get_logger().error(e)

            results = self.yolo.predict(source=image, conf=self.minimum_predict, verbose=False)
            results = results[0].cpu()
            result_img = results.plot()

            if "pose" in self.yolo_model:
                # proseccing
                pa = PersonPose2DArray()
                i = 0
                for box, pred, keypoints in zip(results.boxes.xyxy, results.boxes.conf, results.keypoints):
                    ka = KeyPoint2DArray()
                    
                    if pred is None or keypoints.conf is None:
                        continue
                    for id, (p, conf) in enumerate(zip(keypoints.xy[0], keypoints.conf[0])):
                        k = KeyPoint2D()
                        k.id = id
                        k.predict = float(conf)
                        k.pose.x = int(p[0])
                        k.pose.y = int(p[1])
                        ka.keypoints.append(k)
                    
                    x1, y1, x2, y2 = map(int, box)
                    x = (x2 - x1)//2 + x1
                    y = (y2 - y1)//2 + y1

                    p = PersonPose2D()
                    p.keypoints = ka
                    p.predict = pred.item()
                    p.pose.x = x
                    p.pose.y = y
                    pa.poses.append(p)

                    #self.get_logger().info(f"{p.pose}")
                    #self.get_logger().info(f"{i}\n")
                    i += 1
                    
                pa.header = msg.header
                self._objects_pub.publish(pa)

            else:
                # proseccing
                # get pose and marge data
                object_list = []
                if results.masks:
                    for box, cls, pred, mask in zip(results.boxes.xyxy, results.boxes.cls, results.boxes.conf, results.masks.data.cpu().numpy()):
                        mask = (mask > 0).astype(np.uint8) * 255
                        M = cv2.moments(mask)
                        if M["m00"] != 0:
                            x = int(M["m10"] / M["m00"])
                            y = int(M["m01"] / M["m00"])
                        else:
                            x, y = 0, 0  # モーメントがゼロの場合のフォールバック

                        #debug
                        #cv2.imshow(f'mask_{results.names.get(int(cls))}',mask)

                        o = ObjectPose2D()
                        o.segment_image = self.bridge.cv2_to_imgmsg(mask, encoding="mono8")
                        o.segment_image.header = msg.header
                        o.class_name = results.names.get(int(cls))
                        o.class_id = int(cls)
                        o.predict = pred.item()
                        o.pose.x = x
                        o.pose.y = y
                        object_list.append(o)
                        # debug
                        #cv2.circle(result_img, (x, y), 5, (0, 255, 0), -1)
                else:
                    for box, cls, pred in zip(results.boxes.xyxy, results.boxes.cls, results.boxes.conf):
                        x1, y1, x2, y2 = map(int, box)
                        x = (x2 - x1)//2 + x1
                        y = (y2 - y1)//2 + y1

                        o = ObjectPose2D()
                        o.class_name = results.names.get(int(cls))
                        o.class_id = int(cls)
                        o.predict = pred.item()
                        o.pose.x = x
                        o.pose.y = y
                        object_list.append(o)
                        # debug
                        #cv2.circle(result_img, (x, y), 5, (0, 255, 0), -1)
                
                oa = ObjectPose2DArray()
                oa.header = msg.header
                oa.objects = object_list
                self._objects_pub.publish(oa)

            # debug show
            if self.debug:
                cv2.imshow('YOLOv9', result_img)
                cv2.waitKey(1)

            # detect image publish
            pub_image = self.bridge.cv2_to_imgmsg(result_img, encoding="bgr8")
            pub_image.header = msg.header

            self._image_pub.publish(pub_image)

def main(args=None):
    rclpy.init(args=args)
    node = YoloCore()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy()

if __name__ == "__main__":
    main()
