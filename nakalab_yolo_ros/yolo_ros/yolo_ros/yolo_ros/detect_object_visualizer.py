#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from rclpy.qos import QoSReliabilityPolicy

from visualization_msgs.msg import Marker, MarkerArray
from yolo_ros_msgs.msg import ObjectPose3DArray

import random

class ObjectVisualizer(Node):
    def __init__(self):
        super().__init__('object_visualizer')

        # Qos
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            depth=1
        )

        self.subscription = self.create_subscription(
            ObjectPose3DArray,
            '/yolo_ros/detect_objects/poses',
            self.listener_callback,
            qos_profile)
        self.publisher = self.create_publisher(MarkerArray, '/yolo_ros/detect_objects/markerarray', 10)
        self.get_logger().info(f"Activating {self.get_name()}")

        self.object_colors = {}

    def destroy(self):
        self.get_logger().info(f"Shutdown {self.get_name()}")
        self.destroy_node()

    def get_color_for_object(self, object_id):
        if object_id not in self.object_colors:
            # Generate a random color and store it
            self.object_colors[object_id] = (
                random.random(),  # Red
                random.random(),  # Green
                random.random()   # Blue
            )
        return self.object_colors[object_id]

    def listener_callback(self, msg):
        marker_array = MarkerArray()
        i = 0
        for obj in msg.objects:
            color_r, color_g, color_b = self.get_color_for_object(obj.frame_id)
            
            marker = Marker()
            marker.header = obj.header
            marker.ns = obj.frame_id
            marker.id = i
            marker.type = Marker.CUBE
            marker.action = Marker.ADD
            marker.pose = obj.center
            marker.scale = obj.size
            marker.color.r = color_r
            marker.color.g = color_g
            marker.color.b = color_b
            marker.color.a = 0.3
            marker.lifetime = rclpy.duration.Duration(seconds=0.25).to_msg()
            marker_array.markers.append(marker)

            # Create a marker for the center
            center_marker = Marker()
            center_marker.header = obj.header
            center_marker.ns = obj.frame_id
            center_marker.id = i + 1000  # Ensure unique ID
            center_marker.type = Marker.SPHERE
            center_marker.action = Marker.ADD
            center_marker.pose = obj.center
            center_marker.scale.x = 0.1
            center_marker.scale.y = 0.1
            center_marker.scale.z = 0.1
            center_marker.color.r = 1.0
            center_marker.color.g = 0.0
            center_marker.color.b = 0.0
            center_marker.color.a = 1.0
            center_marker.lifetime = rclpy.duration.Duration(seconds=0.25).to_msg()
            marker_array.markers.append(center_marker)

            i += 1

        self.publisher.publish(marker_array)

def main(args=None):
    rclpy.init(args=args)
    node = ObjectVisualizer()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy()

if __name__ == '__main__':
    main()
