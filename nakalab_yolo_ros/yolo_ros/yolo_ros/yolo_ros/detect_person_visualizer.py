#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from rclpy.qos import QoSReliabilityPolicy
from rclpy.duration import Duration

from visualization_msgs.msg import Marker, MarkerArray
from yolo_ros_msgs.msg import PersonPose3DArray

import random

class PersonVisualizer(Node):
    def __init__(self):
        super().__init__('person_visualizer')

        # Qos
        qos_profile = QoSProfile(
            reliability=QoSReliabilityPolicy.BEST_EFFORT,
            depth=1
        )

        self.subscription = self.create_subscription(
            PersonPose3DArray,
            '/yolo_ros/detect_persons/poses',
            self.listener_callback,
            qos_profile)
        self.publisher = self.create_publisher(MarkerArray, '/yolo_ros/detect_persons/markerarray', 10)
        self.get_logger().info(f"Activating {self.get_name()}")

        self.person_colors = {}
        # COCO keypoints connection list for skeleton
        self.coco_keypoints = {
            0: 'nose', 1: 'left_eye', 2: 'right_eye', 3: 'left_ear', 4: 'right_ear',
            5: 'left_shoulder', 6: 'right_shoulder', 7: 'left_elbow', 8: 'right_elbow', 
            9: 'left_wrist', 10: 'right_wrist', 11: 'left_hip', 12: 'right_hip',
            13: 'left_knee', 14: 'right_knee', 15: 'left_ankle', 16: 'right_ankle'
        }

        self.skeleton_connections = [
            (0, 1), (0, 2),  # nose to eyes
            (1, 3), (2, 4),  # eyes to ears
            (3, 5), (4, 6),  # ears to shoulders
            (5, 6),  # shoulders
            (5, 7), (7, 9),  # left arm
            (6, 8), (8, 10),  # right arm
            (5, 11), (6, 12),  # shoulders to hips
            (11, 12),  # hips
            (11, 13), (13, 15),  # left leg
            (12, 14), (14, 16)  # right leg
        ]

    def destroy(self):
        self.get_logger().info(f"Shutdown {self.get_name()}")
        self.destroy_node()

    def get_color_for_person(self, person_id):
        if person_id not in self.person_colors:
            # Generate a random color and store it
            self.person_colors[person_id] = (
                random.random(),  # Red
                random.random(),  # Green
                random.random()   # Blue
            )
        return self.person_colors[person_id]

    def is_valid_point(self, point):
        return not (point.x == 0.0 and point.y == 0.0 and point.z == 0.0)

    def is_valid_connection(self, keypoints, connection):
        try:
            point1 = keypoints[connection[0]].point
            point2 = keypoints[connection[1]].point
            return self.is_valid_point(point1) and self.is_valid_point(point2)
        except IndexError:
            return False

    def listener_callback(self, msg):
        marker_array = MarkerArray()
        i = 0
        for person in msg.poses:
            # Use person's predict field as an identifier for coloring
            color_r, color_g, color_b = self.get_color_for_person(str(i))

            # Create a marker for the body skeleton
            skeleton_marker = Marker()
            skeleton_marker.header = msg.header
            skeleton_marker.ns = 'skeleton_%d' % i
            skeleton_marker.id = i + 2000  # Ensure unique ID
            skeleton_marker.type = Marker.LINE_LIST
            skeleton_marker.action = Marker.ADD
            skeleton_marker.scale.x = 0.02  # Line width
            skeleton_marker.color.r = color_r
            skeleton_marker.color.g = color_g
            skeleton_marker.color.b = color_b
            skeleton_marker.color.a = 1.0
            skeleton_marker.lifetime = Duration(seconds=0.25).to_msg()

            for connection in self.skeleton_connections:
                if self.is_valid_connection(person.keypoints.points, connection):
                    point1 = person.keypoints.points[connection[0]].point
                    point2 = person.keypoints.points[connection[1]].point
                    skeleton_marker.points.append(point1)
                    skeleton_marker.points.append(point2)

            #self.get_logger().info(f'{skeleton_marker}')
                    
            marker_array.markers.append(skeleton_marker)

            # Create a marker for the center
            color_r, color_g, color_b = self.get_color_for_person(str(i * 1000))
            
            center_marker = Marker()
            center_marker.header = msg.header
            center_marker.ns = 'pc_%d' % i
            center_marker.id = i + 1000  # Ensure unique ID
            center_marker.type = Marker.SPHERE
            center_marker.action = Marker.ADD
            center_marker.pose = person.center
            center_marker.scale.x = 0.1
            center_marker.scale.y = 0.1
            center_marker.scale.z = 0.1
            center_marker.color.r = color_g
            center_marker.color.g = color_b
            center_marker.color.b = color_r
            center_marker.color.a = 1.0
            center_marker.lifetime = Duration(seconds=0.25).to_msg()
            marker_array.markers.append(center_marker)

            # Create markers for the keypoints
            for k in person.keypoints.points:
                keypoint_marker = Marker()
                keypoint_marker.header = msg.header
                keypoint_marker.ns = 'p%d_k%d' % (i, k.id)
                keypoint_marker.id = i * 1000 + k.id  # Ensure unique ID
                keypoint_marker.type = Marker.SPHERE
                keypoint_marker.action = Marker.ADD
                keypoint_marker.pose.position.x = k.point.x
                keypoint_marker.pose.position.y = k.point.y
                keypoint_marker.pose.position.z = k.point.z
                keypoint_marker.scale.x = 0.05
                keypoint_marker.scale.y = 0.05
                keypoint_marker.scale.z = 0.05
                keypoint_marker.color.r = color_b
                keypoint_marker.color.g = color_g
                keypoint_marker.color.b = color_r
                keypoint_marker.color.a = 1.0
                keypoint_marker.lifetime = Duration(seconds=0.25).to_msg()
                marker_array.markers.append(keypoint_marker)
            i += 1

        self.publisher.publish(marker_array)

def main(args=None):
    rclpy.init(args=args)
    node = PersonVisualizer()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy()

if __name__ == '__main__':
    main()
