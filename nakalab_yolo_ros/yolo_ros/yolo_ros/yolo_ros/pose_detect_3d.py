#!/usr/bin/env python3
from rclpy.qos import QoSProfile
from rclpy.qos import QoSHistoryPolicy
from rclpy.qos import QoSDurabilityPolicy
from rclpy.qos import QoSReliabilityPolicy
from rclpy.node import Node
import rclpy

from sensor_msgs.msg import Image, CameraInfo
from yolo_ros_msgs.msg import PersonPose2D, PersonPose2DArray
from yolo_ros_msgs.msg import PersonPose3D, PersonPose3DArray
from yolo_ros_msgs.msg import KeyPoint3D, KeyPoint3DArray

from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2

import message_filters

class PoseDetect3D(Node):
    def __init__(self):
        super().__init__('object_detect_3d')

        # declare variables
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        self.bridge = CvBridge()
        self.depth_image_units_divisor = 1000.0  # 深度画像の単位がミリメートルの場合

        # declare subscribers
        self.depthimage_sub = message_filters.Subscriber(self, Image, "depth_image", qos_profile=qos_profile)
        self.depthinfo_sub = message_filters.Subscriber(self, CameraInfo, "depth_camera_info", qos_profile=qos_profile)
        self.pose_sub = message_filters.Subscriber(self, PersonPose2DArray, "/yolo_ros/detect_persons", qos_profile=qos_profile)

        self._synchronizer = message_filters.ApproximateTimeSynchronizer(
            (self.depthimage_sub, self.depthinfo_sub, self.pose_sub), 10, 0.5)
        self._synchronizer.registerCallback(self._cb)

        # publisher
        self._3d_pose_publish = self.create_publisher(PersonPose3DArray, "/yolo_ros/detect_persons/poses", 10)
        
        self.get_logger().info(f"Activating {self.get_name()}")

    def destroy(self):
        self.get_logger().info(f"Shutdown {self.get_name()}")
        cv2.destroyAllWindows()
        self.destroy_node()

    def _cb(self, depth_msg, info_msg, pose_msg):
        depth_image = self.bridge.imgmsg_to_cv2(depth_msg, "32FC1")

        fx = info_msg.k[0]
        fy = info_msg.k[4]
        cx = info_msg.k[2]
        cy = info_msg.k[5]

        person_pose3d_array = PersonPose3DArray()
        person_pose3d_array.header = pose_msg.header
        i = 0

        for pose in pose_msg.poses:
            keypoints = KeyPoint3DArray()

            points = []
            for keypoint in pose.keypoints.keypoints:
                x_2d = keypoint.pose.x
                y_2d = keypoint.pose.y

                if 0 <= x_2d < depth_image.shape[1] and 0 <= y_2d < depth_image.shape[0]:
                    depth = depth_image[y_2d, x_2d] / self.depth_image_units_divisor
                    if depth > 0:
                        x_3d = (x_2d - cx) * depth / fx
                        y_3d = (y_2d - cy) * depth / fy
                        z_3d = depth

                        k3 = KeyPoint3D()
                        k3.id = keypoint.id
                        k3.predict = keypoint.predict
                        k3.point.x = x_3d
                        k3.point.y = y_3d
                        k3.point.z = z_3d

                        keypoints.points.append(k3)
                        points.append((x_3d, y_3d, z_3d))

            if points:
                # 中心座標の計算
                center_x = sum(p[0] for p in points) / len(points)
                center_y = sum(p[1] for p in points) / len(points)
                center_z = sum(p[2] for p in points) / len(points)

                # サイズの計算
                min_x = min(p[0] for p in points)
                max_x = max(p[0] for p in points)
                min_y = min(p[1] for p in points)
                max_y = max(p[1] for p in points)
                min_z = min(p[2] for p in points)
                max_z = max(p[2] for p in points)

                person_pose3d = PersonPose3D()
                person_pose3d.keypoints = keypoints
                person_pose3d.predict = pose.predict
                person_pose3d.center.position.x = center_x
                person_pose3d.center.position.y = center_y
                person_pose3d.center.position.z = center_z
                #person_pose3d.center.orientation = Quaternion(x=0.0, y=0.0, z=0.0, w=1.0)
                person_pose3d.size.x = max_x - min_x
                person_pose3d.size.y = max_y - min_y
                person_pose3d.size.z = max_z - min_z

                person_pose3d_array.poses.append(person_pose3d)

                #self.get_logger().info(f"{person_pose3d.center}")
                #self.get_logger().info(f"{i}\n")
                i += 1
        
        self._3d_pose_publish.publish(person_pose3d_array)

def main(args=None):
    rclpy.init(args=args)
    node = PoseDetect3D()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy()

if __name__ == "__main__":
    main()
