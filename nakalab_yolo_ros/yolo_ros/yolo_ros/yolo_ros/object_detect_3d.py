#!/usr/bin/env python3
from rclpy.qos import QoSProfile
from rclpy.qos import QoSHistoryPolicy
from rclpy.qos import QoSDurabilityPolicy
from rclpy.qos import QoSReliabilityPolicy
from rclpy.node import Node
import rclpy

from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import Pose, Vector3
from yolo_ros_msgs.msg import ObjectPose2D, ObjectPose2DArray, ObjectPose3D, ObjectPose3DArray

from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2

import message_filters

class ObjectDetect3D(Node):
    def __init__(self):
        super().__init__('object_detect_3d')

        # declare variables
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        self.bridge = CvBridge()
        self.depth_image_units_divisor = 1000.0  # 深度画像の単位がミリメートルの場合

        # declare subscribers
        self.depthimage_sub = message_filters.Subscriber(self, Image, "depth_image", qos_profile=qos_profile)
        self.depthinfo_sub = message_filters.Subscriber(self, CameraInfo, "depth_camera_info", qos_profile=qos_profile)
        self.pose_sub = message_filters.Subscriber(self, ObjectPose2DArray, "/yolo_ros/detect_objects", qos_profile=qos_profile)

        self._synchronizer = message_filters.ApproximateTimeSynchronizer(
            (self.depthimage_sub, self.depthinfo_sub, self.pose_sub), 10, 0.5)
        self._synchronizer.registerCallback(self._cb)

        # publisher
        self._3d_object_publish = self.create_publisher(ObjectPose3DArray, "/yolo_ros/detect_objects/poses", 10)
        
        self.get_logger().info(f"Activating {self.get_name()}")

    def destroy(self):
        self.get_logger().info(f"Shutdown {self.get_name()}")
        cv2.destroyAllWindows()
        self.destroy_node()

    def _cb(self, depth_msg, info_msg, pose_msg):
        depth_image = self.bridge.imgmsg_to_cv2(depth_msg, "32FC1")

        fx = info_msg.k[0]
        fy = info_msg.k[4]
        cx = info_msg.k[2]
        cy = info_msg.k[5]

        op3_array = ObjectPose3DArray()
        for object in pose_msg.objects:
            size = False
            if not bool(object.segment_image.data):
                # object.poseを使って3次元中心座標を計算
                u = object.pose.x
                v = object.pose.y
                d = depth_image[int(v), int(u)] / self.depth_image_units_divisor

                if d > 0:
                    X = (u - cx) * d / fx
                    Y = (v - cy) * d / fy
                    Z = d

                    center = np.array([X, Y, Z])
                    size = [0.0, 0.0, 0.0]
            else:
                mask_image = self.bridge.imgmsg_to_cv2(object.segment_image, 'mono8')
                masked_depth = cv2.bitwise_and(depth_image, depth_image, mask=mask_image)

                valid_pixels = np.where(mask_image > 0)
                u_coords = valid_pixels[1]
                v_coords = valid_pixels[0]
                depths = masked_depth[v_coords, u_coords] / self.depth_image_units_divisor

                # 標準偏差を使ってフィルタリング
                mean_depth = np.mean(depths)
                std_depth = np.std(depths)
                filtered_indices = np.where((depths > mean_depth - std_depth) & (depths < mean_depth + std_depth))
                u_coords = u_coords[filtered_indices]
                v_coords = v_coords[filtered_indices]
                depths = depths[filtered_indices]

                points_3d = []
                for u, v, d in zip(u_coords, v_coords, depths):
                    if d > 0:
                        X = (u - cx) * d / fx
                        Y = (v - cy) * d / fy
                        Z = d
                        points_3d.append([X, Y, Z])

                if points_3d:
                    points_3d = np.array(points_3d)
                    min_point = np.min(points_3d, axis=0)
                    max_point = np.max(points_3d, axis=0)

                    center = (min_point + max_point) / 2.0

                    # PCAによる回転角度の推定
                    mean, eigenvectors = cv2.PCACompute(points_3d, mean=np.array([]))
                    principal_axis = eigenvectors[0]  # 主成分

                    #angle = np.arctan2(principal_axis[1], principal_axis[0])
                    # 水平面での回転角度（yaw）のみを計算
                    yaw = np.arctan2(principal_axis[1], principal_axis[0])

                    size = max_point - min_point

            if type(size) == np.ndarray or size :
                # object message
                object_pose_3d = ObjectPose3D()
                object_pose_3d.header = depth_msg.header
                object_pose_3d.frame_id = object.class_name
                object_pose_3d.class_id = object.class_id
                object_pose_3d.predict = object.predict

                object_pose_3d.center.position.x = center[0]
                object_pose_3d.center.position.y = center[1]
                object_pose_3d.center.position.z = center[2]

                object_pose_3d.center.orientation.x = 0.
                object_pose_3d.center.orientation.y = 0.
                #object_pose_3d.center.orientation.z = np.sin(yaw / 2.0)
                #object_pose_3d.center.orientation.w = np.cos(yaw / 2.0)
                object_pose_3d.center.orientation.z = 0.
                object_pose_3d.center.orientation.w = 1.

                object_pose_3d.size.x = size[0]
                object_pose_3d.size.y = size[1]
                object_pose_3d.size.z = size[2]

                op3_array.objects.append(object_pose_3d)

        #print(len(obj_list))
        op3_array.header = pose_msg.header
        self._3d_object_publish.publish(op3_array)
            
        #cv2.imshow('Masked Depth Image', masked_depth)
        #cv2.waitKey(1)

def main(args=None):
    rclpy.init(args=args)
    node = ObjectDetect3D()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy()

if __name__ == "__main__":
    main()
