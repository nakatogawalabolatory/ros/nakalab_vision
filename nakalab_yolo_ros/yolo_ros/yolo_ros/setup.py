from setuptools import find_packages, setup
from glob import glob
from setuptools import setup
import os

package_name = 'yolo_ros'

data_files = []
data_files.append(("share/ament_index/resource_index/packages", ["resource/" + package_name]))
data_files.append(("share/" + package_name, ["package.xml"]))

def package_files(directory, data_files):
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            data_files.append(("share/" + package_name + "/" + path, glob(path + "/**/*.*", recursive=True)))
    return data_files

# Add directories
data_files = package_files("launch", data_files)
data_files = package_files("rviz", data_files)

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=data_files,
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='roboworks',
    maintainer_email='nakatogawagai@gmail.com',
    description='TODO: Package description',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'yolo_ros_core = yolo_ros.yolo_ros:main',
            'object_detect3d = yolo_ros.object_detect_3d:main',
            'person_detect3d = yolo_ros.pose_detect_3d:main',
            'object_detection_visualizer = yolo_ros.detect_object_visualizer:main',
            'person_detection_visualizer = yolo_ros.detect_person_visualizer:main'
        ],
    },
)
