# yolo_ros
　YOLO による物体検出を実行するコアパッケージです。

## launch 一覧
### [yolo_object_detection.launch.py](launch/yolo_object_detection.launch.py)
```bash
ros2 launch yolo_ros yolo_object_detection.launch.py
```

### [yolo_person_detection.launch.py](launch/yolo_person_detection.launch.py)
```bash
ros2 launch yolo_ros yolo_person_detection.launch.py
```

## ノード一覧
### [yolo_ros_core](yolo_ros/yolo_ros.py)
　yolo_ros のコアノード。このノードで物体検出、人体検出処理を行います。
### [object_detect3d](yolo_ros/object_detect_3d.py)
コアノードから取得した情報から物体の3次元情報を算出するノード。
### [person_detect3d](yolo_ros/pose_detect_3d.py)
コアノードから取得した情報から人体の3次元情報を算出するノード。
### [object_detection_visualizer](yolo_ros/detect_object_visualizer.py)
`object_detect3d`ノードからのトピックを解釈し3次元検出結果を視覚化するノード
### [person_detection_visualizer](yolo_ros/detect_person_visualizer.py)
`person_detect3d`ノードからのトピックを解釈し3次元検出結果を視覚化するノード