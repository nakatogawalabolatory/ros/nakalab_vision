#!/usr/bin/bash
set -e
source $HOME/colcon_ws/install/setup.bash
exec "$@"
