# nakalab_yolo_ros
　**[YOLO](https://docs.ultralytics.com/ja)** を使用した物体、人体検出用パッケージです。このパッケージは Docker で動作することを前提としていますが、依存ライブラリをインストールすればホスト上でも動作します。<br>

> **NVIDIA CUDA 環境で実行してください。CPU はサポートしていません。**

　このパッケージでは以下のコンテナを提供します。

- **yolo_ros_devel**<br>
    開発用コンテナ。自作の YOLO パッケージなどの作成はここで行えます。

- **yolo_ros_object_detection**<br>
    物体検出ノードを起動します。

- **yolo_ros_person_detection**<br>
    人体検出ノードを起動します。

## セットアップ
　本ディレクトリ上で以下のコマンドを実行し、コンテナをビルドします。
```bash
docker compsoe build
```
　ホスト上でこのパッケージをビルドしてください。
```bash
colcon build --symlink-install --packages-up-to yolo_ros
```

## 物体検出を開始する
　物体検出ノード群の起動は、[`docker-compose.yaml`](docker-compose.yaml) の **`yolo_ros_object_detection`** サービスによって起動します。このサービスの `command` フィールドに起動コマンドが記述されています。
```yaml
command: /bin/bash -c "/tmp/ros_entrypoint.sh ros2 launch yolo_ros yolo_object_detection.launch.py image_raw:=/rgb/image_raw depth_image:=/depth/image_raw depth_camera_info:=/depth/camera_info debug:=false"
```
　各引数に使用したいトピック名になるよう書き換えてください。

- **`image_raw:=<検出に使用したい Image トピック>`**
- **`depth_image:=<検出に使用したい深度 Image トピック>`**
- **`depth_camera_info:=<検出に使用したい深度 CameraInfo トピック>`**
- **`debug:=true`にすると検出結果のウィンドウが表示されます**
- **`auto_execute:=true`にすると自動的にYOLO検出プロセスが実行します。**
- **`use_viz:=true`にするとビジュアライズ用Rvizが起動します。**

　サービスの起動は以下のコマンドを実行してください。
```bash
docker compose up yolo_ros_object_detection
```
　`Wait activate for ...` というログが表示されたら、別のターミナルで以下のコマンドを実行し、`yolo_ros_core` に起動サービスを送信してください。起動引数`auto_execute`に`true`を代入したならばこの作業が自動的に行われるためこれをする必要はありません。
```bash
ros2 tservice call /yolo_ros/detect_objects/execute_predict std_srvs/SetBool "{data: true}"
```
　`Activating ...` と表示されたら物体検出ノードが起動します。これらのノード群は以下のトピックをパブリッシュします。

- `/yolo_ros/detect_persons/image` [**sensor_msgs/msg/Image**](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)<br>
    検出結果をプロットした画像データ
- `/yolo_ros/detect_persons` [**yolo_ros_msgs/msg/PersonPose2DArray**](yolo_ros/yolo_ros_msgs/msg/PersonPose2DArray.msg)<br>
    2次元画像ピクセル座標の物体検出情報
- `/yolo_ros/detect_persons/poses` [**yolo_ros_msgs/msg/PersonPose3DArray**](yolo_ros/yolo_ros_msgs/msg/PersonPose3DArray.msg)<br>
    3次元ワールド座標の物体検出情報
- `/yolo_ros/detect_persons/markerarray` [**visualization_msgs/msg/MarkerArray**](http://docs.ros.org/en/noetic/api/visualization_msgs/html/msg/MarkerArray.html)<br>
    検出結果を **RViz** で視覚化

> `yolo_ros_msgs` が提供するメッセージのリファレンスは **[yolo_ros_msgs の README](yolo_ros/yolo_ros_msgs/)** を参照してください。

## 人体検出を開始する
　物体検出ノード群の起動は、[`docker-compose.yaml`](docker-compose.yaml) の **`yolo_ros_person_detection`** サービスによって起動します。このサービスの `command` フィールドに起動コマンドが記述されています。
```yaml
command: /bin/bash -c "/tmp/ros_entrypoint.sh ros2 launch yolo_ros yolo_person_detection.launch.py image_raw:=/rgb/image_raw depth_image:=/depth/image_raw depth_camera_info:=/depth/camera_info debug:=false"
```
　各引数に使用したいトピック名になるよう書き換えてください。

- **`image_raw:=<検出に使用したい Image トピック>`**
- **`depth_image:=<検出に使用したい深度 Image トピック>`**
- **`depth_camera_info:=<検出に使用したい深度 CameraInfo トピック>`**
- **`debug:=true`にすると検出結果のウィンドウが表示されます**
- **`auto_execute:=true`にすると自動的にYOLO検出プロセスが実行します。**
- **`use_viz:=true`にするとビジュアライズ用Rvizが起動します。**

　サービスの起動は以下のコマンドを実行してください。
```bash
docker compose up yolo_ros_person_detection
```
　`Wait activate for ...` というログが表示されたら、別のターミナルで以下のコマンドを実行し、`yolo_ros_core` に起動サービスを送信してください。起動引数`auto_execute`に`true`を代入したならばこの作業が自動的に行われるためこれをする必要はありません。
```bash
ros2 tservice call /yolo_ros/detect_persons/execute_predict std_srvs/SetBool "{data: true}"
```
　`Activating ...` と表示されたら物体検出ノードが起動します。これらのノード群は以下のトピックをパブリッシュします。

- `/yolo_ros/detect_persons/image` [**sensor_msgs/msg/Image**](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)<br>
    検出結果をプロットした画像データ
- `/yolo_ros/detect_persons` [**yolo_ros_msgs/msg/PersonPose2DArray**](yolo_ros/yolo_ros_msgs/msg/PersonPose2DArray.msg)<br>
    2次元画像ピクセル座標の物体検出情報
- `/yolo_ros/detect_persons/poses` [**yolo_ros_msgs/msg/PersonPose3DArray**](yolo_ros/yolo_ros_msgs/msg/PersonPose3DArray.msg)<br>
    3次元ワールド座標の物体検出情報
- `/yolo_ros/detect_persons/markerarray` [**visualization_msgs/msg/MarkerArray**](http://docs.ros.org/en/noetic/api/visualization_msgs/html/msg/MarkerArray.html)<br>
    検出結果を **RViz** で視覚化

> `yolo_ros_msgs` が提供するメッセージのリファレンスは **[yolo_ros_msgs の README](yolo_ros/yolo_ros_msgs/)** を参照してください。

## RViz で検出結果を視覚化する
　両サービスを起動すると
```
/yolo_ros/detect_XXX/markerarray
```
という `MarkerArray` トピックがパブリッシュされます。RViz でこのトピックを追加すると検出結果が視覚化されます。

## ホスト上で実行する場合
　Docker コンテナを使用せず、ホスト上でこのパッケージを動かすには、事前に以下の環境構築作業を実行する必要があります。
```bash
# python 3.10 venv 環境をインストール
sudo apt install -y python3.10-venv

# ROS _ws/src 外に以下のリポジトリをクローン
git clone https://github.com/sujanshresstha/YOLOv9_DeepSORT.git; cd YOLOv9_DeepSORT

# python3.10 環境で依存関係をインストール
python3 -m venv venv; source venv/bin/activate
pip install -r requirements.txt
git clone https://github.com/WongKinYiu/yolov9.git
cd yolov9; pip install -r requirements.txt

# YOLO をインストール
pip install ultralytics

# このパッケージ内にウェイトデータをダウンロード（任意）
mkdir weights
wget -P weights https://github.com/WongKinYiu/yolov9/releases/download/v0.1/yolov9-e.pt
```
本パッケージをビルドしたら、`ros2 launch` コマンドでノードを起動できるようになります。
```bash
colcon build --symlink-install --packages-up-to yolo_ros
```
```bash
# 物体検出
ros2 launch yolo_ros yolo_object_detection.launch.py
# 人体検出
ros2 launch yolo_ros yolo_person_detection.launch.py
```
