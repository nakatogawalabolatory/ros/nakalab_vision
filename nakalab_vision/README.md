# nakalab_vision
　深度カメラのパラメータと起動ファイルを提供するパッケージです。このパッケージでは

- Intel RealSense D4535
- ASUS Xtion

カメラをサポートし、両カメラの起動を管理します。

## setup
　このパッケージをインストールしたワークスペース上で `rosdep` を使い依存関係を解決してください。
```bash
# in _ws
rosdep install -y -i --from-path .
```
## launch
### Realsense を起動する
　本パッケージの以下の launch ファイルを起動すると Realsense D435 を起動することができます。
```bash
ros2 launch nakalab_vision realsense.launch.py
```
　この起動ファイルは以下の引数を持ちます。

- **`rviz_view`**<br>
    デフォルト値；`false`<br>
    ビジュアライザー用 RViz を起動します。

### Xtion RGB-D PRO を起動する
　本パッケージの以下の launch ファイルを起動すると Xtion RGB-D PRO を起動することができます。
```bash
ros2 launch nakalab_vision xtion.launch.py
```
　この起動ファイルは以下の引数を持ちます。

- **`camera_viewer`**<br>
    デフォルト値；`false`<br>
    ビジュアライザー用 RViz を起動します。
- **`xtion_namespace`**<br>
    デフォルト値；`''`<br>
    このカメラの名前空間を定義します。

### Xtion カメラモデルを起動する
　[xtion_spawn.launch](launch/xtion_spawn.launch) によってカメラモデルをインクルード、起動します。この起動ファイルはあくまでサンプル用なので、このファイルを参考にし [xtion_spawner.urdf.xacro](urdf/xtion_spawner.urdf.xacro)
を編集してください。<br>
　この`xacro`ファイルの以下の部分にある`name`フィールドにカメラ起動ファイルで指定した引数`xtion_namespace`と同じ名前を記述してください。
```xml
  <xacro:asus_camera name="openni" parent="_base" >  
    <origin xyz="0 0 1.0" rpy="0 0 1.57"/>
  </xacro:asus_camera>
```

### Xtion PRO を起動する
　本パッケージの以下の launch ファイルを起動すると Xtion PRO を起動することができます。この起動ファイルは深度画像のみ取得し、RGB 画像は取得しません。
```bash
ros2 launch nakalab_vision xtion_depth_only.launch.py
```
　この起動ファイルは以下の引数を持ちます。

- **`camera_viewer`**<br>
    デフォルト値；`false`<br>
    ビジュアライザー用 RViz を起動します。
- **`xtion_namespace`**<br>
    デフォルト値；`''`<br>
    このカメラの名前空間を定義します。