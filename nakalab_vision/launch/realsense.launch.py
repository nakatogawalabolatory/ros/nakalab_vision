import os
import yaml
from launch import LaunchDescription
import launch_ros.actions
from launch.actions import DeclareLaunchArgument, OpaqueFunction
from launch.substitutions import LaunchConfiguration
from ament_index_python.packages import get_package_share_directory
from launch.conditions import IfCondition, UnlessCondition

def generate_launch_description():
    
    return LaunchDescription([
        DeclareLaunchArgument(
            'rviz_view',
            default_value='false',
            description='If you want Rviz to display camera view and PointCloud, please set the value to true'
        ),
        launch_ros.actions.Node(
            package='realsense2_camera',
            namespace='/realsense',
            name='realsense_node',
            executable='realsense2_camera_node',
            parameters=[os.path.join(get_package_share_directory(
                'nakalab_vision'), 'config', 'realsense.yaml')],
            emulate_tty=True,
            output='screen',
        ),
        launch_ros.actions.Node(
            package='rviz2',
            executable='rviz2',
            condition=IfCondition(LaunchConfiguration('rviz_view')),
            arguments=[
                '-d', os.path.join(
                    get_package_share_directory('nakalab_vision'),
                    'rviz', 'realsense_view.rviz'
                )
            ]
        )
    ])
