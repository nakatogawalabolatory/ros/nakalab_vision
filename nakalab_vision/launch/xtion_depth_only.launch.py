import launch
import launch_ros.actions
import launch_ros.descriptions
from launch.substitutions import LaunchConfiguration
from launch.substitutions import ThisLaunchFileDir
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch_xml.launch_description_sources import XMLLaunchDescriptionSource

from ament_index_python.packages import get_package_share_directory
import os

def generate_launch_description():
    namespace = LaunchConfiguration('xtion_namespace')

    container = launch_ros.actions.ComposableNodeContainer(
            name='container',
            namespace=namespace,
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                # Driver
                launch_ros.descriptions.ComposableNode(
                    package='openni2_camera',
                    plugin='openni2_wrapper::OpenNI2Driver',
                    name='driver',
                    namespace=namespace,
                    parameters=[
                        {'depth_registration': True},
                        {'use_device_time': True},
                        {'rgb_camera_info_url': 'file://' + os.path.join(get_package_share_directory('nakalab_vision'),'config','rgb_PS1080_PrimeSense.yaml')},
                        {'depth_camera_info_url': 'file://' + os.path.join(get_package_share_directory('nakalab_vision'),'config','rgb_PS1080_PrimeSense.yaml')}
                    ],
                    remappings=[('depth/image', 'depth_registered/image_raw')],
                ),
                # Create XYZ point cloud
                launch_ros.descriptions.ComposableNode(
                    package='depth_image_proc',
                    plugin='depth_image_proc::PointCloudXyzNode',
                    name='points_xyz',
                    namespace=namespace,
                    parameters=[{'queue_size': 10}],
                    remappings=[
                        ('image_rect', 'depth/image_raw'),
                        ('camera_info', 'depth/camera_info'),
                        ('points', 'depth_registered/points')
                    ],
                ),
            ],
            output='screen',
    )

    # Include robot description launch
    description = IncludeLaunchDescription(
        XMLLaunchDescriptionSource([
            ThisLaunchFileDir(), '/xtion_spawn.launch'
        ]),
        condition=IfCondition(LaunchConfiguration('camera_viewer'))
    )


    return launch.LaunchDescription([
            DeclareLaunchArgument(
                'camera_viewer',
                default_value='false',
                description='If you want Rviz to display camera view and PointCloud, please set the value to true'
            ),
            DeclareLaunchArgument(
                'xtion_namespace',
                default_value='',
                description='Xtion camera nodes namespace'
            ),

            container,
            launch_ros.actions.Node(
                package='rviz2',
                executable='rviz2',
                condition=IfCondition(LaunchConfiguration('camera_viewer')),
                arguments=[
                    '-d', os.path.join(
                    get_package_share_directory('nakalab_vision'),
                    'rviz', 'xtion_depth_only_view.rviz'
                )]
            ),
            description
        ])
