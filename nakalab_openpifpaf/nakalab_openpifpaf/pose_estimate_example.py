#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np

import torch

import openpifpaf
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas # visualize for openpifpaf

class CameraViewer(Node):

    def __init__(self):
        super().__init__('pose_estimate_example')
        self.subscription = self.create_subscription(
            Image,
            '/realsense/color/image_raw',
            self.image_callback,
            10)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()

        # use gpu or cpu
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        # OpenPifpaf
        self.predictor = openpifpaf.Predictor(checkpoint='shufflenetv2k16')
        #self.predictor = openpifpaf.Predictor(checkpoint='shufflenetv2k30-apollo-66')
        self.predictor.device = self.device
        self.annotation_painter = openpifpaf.show.AnnotationPainter()

    def image_callback(self, msg):
        try:
            # Convert ROS Image message to OpenCV image
            cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            self.get_logger().error("CvBridge Error: {0}".format(e))

        # Pose estimation
        predictions, _, _ = self.predictor.numpy_image(cv_image)

        # Draw poses on image
        with openpifpaf.show.image_canvas(cv_image) as ax:
            self.annotation_painter.annotations(ax, predictions)
        figure_canvas = FigureCanvas(ax.figure)
        figure_canvas.draw()
        cv_image = np.array(figure_canvas.renderer.buffer_rgba())

        # Display image
        cv2.imshow("Camera View", cv_image)
        cv2.waitKey(1)

def main(args=None):
    rclpy.init(args=args)
    camera_viewer = CameraViewer()
    try:
        rclpy.spin(camera_viewer)
    except KeyboardInterrupt:
        pass
    finally:
        # Clean up
        camera_viewer.destroy_node()
        cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
