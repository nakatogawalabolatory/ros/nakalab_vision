# nakalab_demo
```
ros2 run nakalab_demo demo_rgb_from_pc2 --ros-args --remap rgbd/points:=<RGBD-TOPIC>
```
　このノードは RGB PointCloud2 データから RGB画像を取得するサンプルコードです。[ソースコード](src/demo_rgb_from_pc2.cpp)
