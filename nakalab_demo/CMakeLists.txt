cmake_minimum_required(VERSION 3.5)
project(nakalab_demo)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(pcl_conversions REQUIRED)
find_package(pcl_ros REQUIRED)
find_package(cv_bridge REQUIRED)
find_package(OpenCV REQUIRED)

# Add executable
add_executable(demo_rgb_from_pc2 src/demo_rgb_from_pc2.cpp)
ament_target_dependencies(demo_rgb_from_pc2 rclcpp sensor_msgs pcl_conversions pcl_ros cv_bridge OpenCV)

install(TARGETS
  demo_rgb_from_pc2
  DESTINATION lib/${PROJECT_NAME}
)

ament_package()
