#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/image_encodings.hpp>
#include <opencv2/opencv.hpp>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <cv_bridge/cv_bridge.h>

class RGBPointCloudToImages : public rclcpp::Node
{
public:
    RGBPointCloudToImages() : Node("rgb_pointcloud_to_images")
    {
        rclcpp::QoS qos(rclcpp::KeepLast(10));
        qos.best_effort();
        
        subscription_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
            "/rgbd/points", qos,
            std::bind(&RGBPointCloudToImages::pointcloud_callback, this, std::placeholders::_1));
        
        RCLCPP_INFO(this->get_logger(), "RGB PointCloud to Images node has been started.");
    }

private:
    void pointcloud_callback(const sensor_msgs::msg::PointCloud2::SharedPtr msg)
    {
        // Convert PointCloud2 to PCL point cloud
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::fromROSMsg(*msg, *cloud);

        // Extract RGB and depth data
        cv::Mat rgb_image(msg->height, msg->width, CV_8UC3);
        cv::Mat depth_image(msg->height, msg->width, CV_32FC1);
        for (size_t i = 0; i < msg->width * msg->height; ++i)
        {
            rgb_image.at<cv::Vec3b>(i / msg->width, i % msg->width)[0] = cloud->points[i].b;
            rgb_image.at<cv::Vec3b>(i / msg->width, i % msg->width)[1] = cloud->points[i].g;
            rgb_image.at<cv::Vec3b>(i / msg->width, i % msg->width)[2] = cloud->points[i].r;
            depth_image.at<float>(i / msg->width, i % msg->width) = cloud->points[i].z;
        }

        // Display the RGB image using OpenCV
        cv::imshow("RGB Image", rgb_image);
        cv::waitKey(1);
    }

    rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr subscription_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<RGBPointCloudToImages>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
