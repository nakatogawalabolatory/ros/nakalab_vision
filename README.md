# nakalab_vision
　インテリジェンスビジョンパッケージ。<br>

## [nakalab_vision](nakalab_vision)
　Depth カメラの起動を司ります。詳しくは [README](nakalab_vision/README.md) をご覧ください。

## [nakalab_yolo_ros](nakalab_yolo_ros)
　**[YOLO](https://docs.ultralytics.com/ja)**
を使用した物体、人体検出パッケージです。詳しくは [README](nakalab_yolo_ros/README.md) をご覧ください。

## [nakalab_openpifpaf](nakalab_openpifpaf) `開発中`
　**[OpenPifpaf](https://openpifpaf.github.io/intro.html)**
を使用した人体検出パッケージです。詳しくは [README](nakalab_openpifpaf/README.md) をご覧ください。

## [nakalab_demo](nakalab_demo) `開発中`
　本パッケージ群を使用した各種デモノードを提供します。詳しくは [README](nakalab_demo/README.md) をご覧ください。
